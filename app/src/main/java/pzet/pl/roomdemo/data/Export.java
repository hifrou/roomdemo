package pzet.pl.roomdemo.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.util.Date;

import pzet.pl.roomdemo.room.DateConverter;

@Entity(tableName = "export")
@TypeConverters(DateConverter.class)
public class Export {

    public Export(@NonNull String exportName, @NonNull String userName, @NonNull String placeName) {
        this.exportName = exportName;
        this.userName = userName;
        this.placeName = placeName;
        this.creationDate = new Date().getTime();
    }

    @Ignore
    public Export(@NonNull String exportName, @NonNull String userName, @NonNull String placeName, long creationDate) {
        this.exportName = exportName;
        this.userName = userName;
        this.placeName = placeName;
        this.creationDate = creationDate;
    }

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int _id;

    @NonNull
    private String exportName;

    @NonNull
    private String userName;

    @NonNull
    private String placeName;

    @NonNull
    private long creationDate;

    public int get_id() {
        return _id;
    }

    @NonNull
    public String getExportName() {
        return exportName;
    }

    @NonNull
    public String getUserName() {
        return userName;
    }

    @NonNull
    public String getPlaceName() {
        return placeName;
    }

    @NonNull
    public long getCreationDate() {
        return creationDate;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void setExportName(@NonNull String exportName) {
        this.exportName = exportName;
    }

    public void setUserName(@NonNull String userName) {
        this.userName = userName;
    }

    public void setPlaceName(@NonNull String placeName) {
        this.placeName = placeName;
    }

    public void setCreationDate(@NonNull long creationDate) {
        this.creationDate = creationDate;
    }
}