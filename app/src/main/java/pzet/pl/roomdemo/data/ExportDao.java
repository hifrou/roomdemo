package pzet.pl.roomdemo.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;
import java.util.List;

import pzet.pl.roomdemo.room.DateConverter;

@Dao
public interface ExportDao {

    @Insert
    void insert(Export export);

    @Query("DELETE FROM export")
    void deleteAll();

    @Query("SELECT * from export ORDER BY exportName ASC")
    LiveData<List<Export>> getAllExports();

    @Query("SELECT * FROM export WHERE exportName == :query")
    LiveData<List<Export>> fetchExportsByName(String query);

    @Query("SELECT * FROM export WHERE userName == :query")
    LiveData<List<Export>> fetchExportsByUser(String query);

    @Query("SELECT * FROM export WHERE placeName == :query")
    LiveData<List<Export>> fetchExportsByPlace(String query);

}
