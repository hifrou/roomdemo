package pzet.pl.roomdemo.room;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.Date;

import pzet.pl.roomdemo.data.Export;
import pzet.pl.roomdemo.data.ExportDao;


@android.arch.persistence.room.Database(entities = {Export.class}, version = 1)
public abstract class Database extends RoomDatabase {

    private static volatile Database INSTANCE;

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final ExportDao mDao;

        PopulateDbAsync(Database db) {
            mDao = db.exportDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            mDao.deleteAll();
            Export export = new Export("test1", "user1", "factory");
            mDao.insert(export);
            export = new Export("test2", "user1", "shop");
            mDao.insert(export);
            export = new Export("test3", "user2", "factory");
            mDao.insert(export);
            export = new Export("test4", "user2", "office");
            mDao.insert(export);
            export = new Export("test5", "user2", "office", new Date("19/02/2019  12:13:50").getTime());
            mDao.insert(export);
            return null;
        }
    }

    public static Database getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (Database.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            Database.class, "database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract ExportDao exportDao();

}
