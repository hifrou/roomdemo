package pzet.pl.roomdemo.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import pzet.pl.roomdemo.R;
import pzet.pl.roomdemo.data.Export;

public class ExportListAdapter extends RecyclerView.Adapter<ExportListAdapter.ExportViewHolder> {

    class ExportViewHolder extends RecyclerView.ViewHolder {
        private final TextView exportName;
        private final TextView exportDate;
        private final TextView exportTime;
        private final TextView userName;
        private final TextView placeName;

        private ExportViewHolder(View itemView) {
            super(itemView);
            exportName = itemView.findViewById(R.id.exportName);
            exportDate = itemView.findViewById(R.id.exportDate);
            exportTime = itemView.findViewById(R.id.exportTime);
            userName = itemView.findViewById(R.id.userName);
            placeName = itemView.findViewById(R.id.placeName);
        }
    }

    private final LayoutInflater mInflater;
    private List<Export> exports; // Cached copy of words

    ExportListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public ExportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new ExportViewHolder(itemView);
    }

    private final DateFormat timeFormat = new SimpleDateFormat("hh:mm", new Locale("PL"));
    private final DateFormat dateFormat = new SimpleDateFormat("dd-MM", new Locale("PL"));

    @Override
    public void onBindViewHolder(ExportViewHolder holder, int position) {
        if (exports != null) {

            Export current = exports.get(position);
            holder.exportName.setText(current.getExportName());
            holder.exportDate.setText(dateFormat.format(current.getCreationDate()));
            holder.exportTime.setText(timeFormat.format(current.getCreationDate()));
            holder.userName.setText(current.getUserName());
            holder.placeName.setText(current.getPlaceName());
        } else {
            // Covers the case of data not being ready yet.
            holder.exportName.setText("No export");
        }
    }

    void setExports(List<Export> words){
        exports = words;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mUsers has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (exports != null)
            return exports.size();
        else return 0;
    }
}
