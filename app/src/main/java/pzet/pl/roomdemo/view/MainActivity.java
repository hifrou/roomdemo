package pzet.pl.roomdemo.view;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;

import pzet.pl.roomdemo.R;
import pzet.pl.roomdemo.data.Export;
import pzet.pl.roomdemo.viewModel.ExportViewModel;

public class MainActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    private ExportViewModel exportViewModel;

    private TextView exportName;
    private TextView date;
    private TextView time;
    private TextView userName;
    private TextView placeName;

    private ExportListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        adapter = new ExportListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        exportViewModel = ViewModelProviders.of(this).get(ExportViewModel.class);

        exportViewModel.getAllExports().observe(this, new Observer<List<Export>>() {
            @Override
            public void onChanged(@Nullable final List<Export> exports) {
                // Update the cached copy of the words in the adapter.
                for(Export e : exports){
                    Log.d("created: ", String.valueOf(e.getCreationDate()));
                }
                adapter.setExports(exports);
            }
        });

        exportName = findViewById(R.id.headerExportName);
        date = findViewById(R.id.headerExportDate);
        time = findViewById(R.id.headerExportTime);
        userName = findViewById(R.id.headerUserName);
        placeName = findViewById(R.id.headerPlaceName);

        time.setOnClickListener(v -> new TimePickerDialog(
                MainActivity.this,
                MainActivity.this::onTimeSet,
                LocalDateTime.now().getHour(),
                LocalDateTime.now().getMinute(),
                true).show());
        date.setOnClickListener(v -> new DatePickerDialog(
                MainActivity.this,
                MainActivity.this::onDateSet,
                LocalDateTime.now().getYear(),
                LocalDateTime.now().getMonth().getValue()-1,
                LocalDateTime.now().getDayOfMonth()).show());

        exportName.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Title");
            final EditText input = new EditText(this);

            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);

            builder.setPositiveButton("OK", (dialog, which) -> {
                if (adapter != null && exportViewModel != null) {
                    List<Export> filteredResults =
                            exportViewModel.getExportsByName(input.getText().toString());
                    adapter.setExports(filteredResults);
                }
            });
            builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
            builder.show();
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (adapter != null && exportViewModel != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, dayOfMonth, 0, 0);
            List<Export> filteredResults =
                    exportViewModel.getExportsByDay(
                            calendar.getTime());
            adapter.setExports(filteredResults);
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if (adapter != null && exportViewModel != null) {
            List<Export> filteredResults =
                    exportViewModel.getExportsByHourAndMinute(hourOfDay, minute);
            adapter.setExports(filteredResults);
        }    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_reset_filters) {
            if (adapter != null && exportViewModel != null) {
                adapter.setExports(exportViewModel.getAllExports().getValue());
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
