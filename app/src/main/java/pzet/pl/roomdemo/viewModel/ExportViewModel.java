package pzet.pl.roomdemo.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pzet.pl.roomdemo.data.Export;

public class ExportViewModel extends AndroidViewModel {

    private Repository mRepository;

    private LiveData<List<Export>> allExports;


    public ExportViewModel(Application application) {
        super(application);
        mRepository = new Repository(application);
        allExports = mRepository.getAllExports();
    }

    public LiveData<List<Export>> getAllExports() {
        return allExports;
    }

    public void insert(Export word) {
        mRepository.insert(word);
    }

    public List<Export> getExportsByDay(Date day) {
        long dayStart, dayEnd;
        Calendar cal = Calendar.getInstance();
        cal.setTime(day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        dayStart = cal.getTime().getTime();

        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);

        dayEnd = cal.getTime().getTime();
        Log.e("start ", String.valueOf(dayStart));
        Log.e("end   ", String.valueOf(dayEnd));

        List<Export> filtered = new ArrayList<>();
        for (Export export : allExports.getValue()) {
            if (export.getCreationDate() >= dayStart && export.getCreationDate() <= dayEnd) {
                filtered.add(export);
            }
        }
        return filtered;
    }

    public List<Export> getExportsByUser(String userName) {
        List<Export> filtered = new ArrayList<>();
        for (Export export : allExports.getValue()) {
            if (export.getUserName().equals(userName)) {
                filtered.add(export);
            }
        }
        return filtered;
    }

    public List<Export> getExportsByName(String exportName) {
        List<Export> filtered = new ArrayList<>();
        for (Export export : allExports.getValue()) {
            if (export.getExportName().equals(exportName)) {
                filtered.add(export);
            }
        }
        return filtered;
    }

    public List<Export> getExportsByPlace(String placeName) {
        List<Export> filtered = new ArrayList<>();
        for (Export export : allExports.getValue()) {
            if (export.getPlaceName().equals(placeName)) {
                filtered.add(export);
            }
        }
        return filtered;
    }

    public List<Export> getExportsByHourAndMinute(int hourOfDay, int minute) {
        //TODO: implement me
        return null;
    }
}
