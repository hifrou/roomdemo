package pzet.pl.roomdemo.viewModel;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

import pzet.pl.roomdemo.data.Export;
import pzet.pl.roomdemo.data.ExportDao;
import pzet.pl.roomdemo.room.Database;

public class Repository {

    private ExportDao exportDao;
    private LiveData<List<Export>> allExports;
    private LiveData<List<Export>> filteredExports;


    public Repository(Application application) {
        Database db = Database.getDatabase(application);
        exportDao = db.exportDao();
    }

    public LiveData<List<Export>> getAllExports() {
        allExports = exportDao.getAllExports();
        return allExports;
    }

    public LiveData<List<Export>> getExportsByName(String name){
        return exportDao.fetchExportsByName(name);
    }

    public LiveData<List<Export>> getExportsByUser(String userName){
        return exportDao.fetchExportsByName(userName);
    }


    public LiveData<List<Export>> getExportsByPlace(String placeName){
        return exportDao.fetchExportsByName(placeName);
    }


    public void insert (Export export) {
        new insertAsyncTask(exportDao).execute(export);
    }

    private static class insertAsyncTask extends AsyncTask<Export, Void, Void> {

        private ExportDao mAsyncTaskDao;

        insertAsyncTask(ExportDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Export... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
